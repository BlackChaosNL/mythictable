param(
	[switch] $isFirstTime
)

$originalDir = Split-Path -Parent $PSCommandPath

$clientPath = $originalDir + "/html"
$clientPort = 5000
$clientUrl = "http://localhost:${clientPort}/"

$authPort = 5002

$serverPath = $originalDir + "/server/src/MythicTable"
$serverPort = 5001

function Install-Dependencies() {
	Write-Host "Setting up for the first time.... Ctrl-C to Abort"
	Start-Sleep -Seconds 5
	Set-Location $serverPath
	Test-Dotnet
	dotnet dev-certs https
	dotnet dev-certs https --trust
	Set-Location $clientPath
	Test-Npm
	npm install
	npm install vue-cli
	Set-Location $originalDir
	Write-Host "We have finished setup. Press Enter to run the app."
	Read-Host
}

function Test-Dotnet() {
	$dotnetVersion = Get-Command dotnet.exe | Select-Object Version
	if (-Not ($dotnetVersion.Version.Major -ge 3)) {
		Write-Error ".NET core 3.0 or higher required. We checked 'Get-Command dotnet.exe | Select-Object Version', if this is erroneous you may have to manually start your environment."
	}
}

function Test-Npm() {
	if (-Not (Get-Command npm)) {
		Write-Error "npm required, please install npm and try again."
	}
}

function Test-Dependencies() {
	Test-Dotnet
	Test-Npm
}

function Start-VueCli() {
	if (Test-NetConnection "localhost" -Port $clientPort -InformationLevel quiet) {
		Write-Warning "Client development environment is already running at http://localhost:${clientPort}"
		return
	}

	Start-Process "npm" -ArgumentList "run","serve" -WorkingDirectory $clientPath
}

function Start-MythicTable() {
	if (Test-NetConnection "localhost" -Port $serverPort -InformationLevel Quiet) {
		Write-Warning "Server development environment is already running at https://localhost:${serverPort}"
		return $true
	}

	Start-Process "dotnet" -ArgumentList "watch","run" -WorkingDirectory $serverPath
}


if ($isFirstTime) {
	Install-Dependencies
}

Write-Host "Checking Dependencies..."
Test-Dependencies
Write-Host "Testing connection to Auth Server..."
if ((Test-NetConnection "localhost" -Port $authPort).TcpTestSucceeded -ne $true) {
	Write-Error "Unable to connect to Auth Server on port $authPort"
	exit
}
Write-Host "Starting Mythic Table BackEnd server..."
Start-MythicTable
Write-Host "Starting Mythic Table FrontEnd server..."
Start-VueCli

Write-Host "Attempting to connect to Mythic Table..."
# Wait for client server to accept connections
$timeout = (Get-Date).AddMinutes(1)
do {
	if ((Get-Date) -gt $timeout) {
		Write-Error "vue-cli did not start or is refusing connections" `
			-RecommendedAction "Check the output of the npm console for details. Maybe run 'npm install vue-cli'?"
	}

	Start-Sleep -Seconds 2;
} until (Test-NetConnection "localhost" -Port $clientPort -InformationLevel quiet)

# Write-Host "Opening client (${clientUrl})..."
# Start-Process $clientUrl
