import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store.js';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';

Vue.use(VueRouter);

const routes = [
    {
        path: '/play/:sessionId/:sceneId',
        name: 'live-play',
        component: () => import('./views/LivePlay'),
        props: true,
    },
    {
        path: '/$debug/*',
        component: () => import('./debug/DebugPage.vue'),
    },
    {
        path: '/$debug',
        redirect: '/$debug/',
    },
    {
        path: '/home',
        component: () => import('./Home.vue'),
        meta: { isPublic: true },
    },
    {
        path: '/oidc',
        name: 'oidcCallback',
        component: () => import('./OidcCallback.vue'),
    },
    {
        path: '/',
        name: 'layout',
        component: () => import('./layout/TheLayout'),
        children: [
            {
                path: 'invite/:id',
                component: () => import('./campaign-management/CampaignInvitation'),
                name: 'campaign-invite',
            },
            {
                path: 'edit/:id?',
                component: () => import('./campaign-management/CampaignEdit'),
                name: 'campaign-edit',
                alias: 'create',
            },
            {
                path: '*',
                component: () => import('./campaign-management/CampaignList'),
                name: 'campaign-list',
            },
        ],
    },
];

const router = new VueRouter({
    routes,
    mode: 'history',
});
router.beforeEach(vuexOidcCreateRouterMiddleware(store));

export default router;
