using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable;
using MythicTable.Campaign.Data;
using MythicTable.GameSession;
using Newtonsoft.Json.Linq;
using Xunit;

namespace LivePlayTests
{
    // TODO #6: Test GameState
    public class LivePlayHubTests
    {
        public Mock<IEntityCollection> stateMock;
        public Mock<IGameState> gameStateMock;
        public Mock<ICampaignProvider> campaignProviderMock;
        public Mock<ILogger<LivePlayHub>> loggerMock;
        public Mock<IHubCallerClients<ILiveClient>> clientsMock;
        public Mock<ILiveClient> allClientsMock;

        public LivePlayHub hub;

        public LivePlayHubTests()
        {
            stateMock = new Mock<IEntityCollection>();
            gameStateMock = new Mock<IGameState>();
            campaignProviderMock = new Mock<ICampaignProvider>();
            loggerMock = new Mock<ILogger<LivePlayHub>>();
            clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            allClientsMock = new Mock<ILiveClient>();
            
            hub = new LivePlayHub(stateMock.Object, gameStateMock.Object, campaignProviderMock.Object, loggerMock.Object);
        }

        [Fact]
        public async Task GameStepIsSubmittedToStateManagerOnce()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(Mock.Of<ILiveClient>());
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            stateMock.Verify(
                entities => entities.ApplyDelta(
                    It.Is<IEnumerable<EntityOperation>>(step => step == submittedStep.Entities)),
                Times.Once);
        }

        [Fact]
        public async Task GameStepIsRepeatedToAllClientsOnce()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Once());
        }

        [Fact]
        public async Task FailedSubmissionsAreNotRepeated()
        {
            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(false));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Never);
        }

        [Fact]
        public async Task ValidDiceRollsAreExecuted()
        {
            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);

            allClientsMock
                .Setup(a => a.ReceiveDiceResult(It.IsAny<RollDTO>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;
            hub.Context = Mock.Of<HubCallerContext>();

            var roll = new RollDTO
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "1d5"
            };

            await hub.RollDice(roll);

            allClientsMock.Verify(
                c => c.ReceiveDiceResult(It.Is<RollDTO>(rolled => rolled.Result != "Invalid dice roll" && rolled.ClientId == "123" && rolled.SessionId == "test" && rolled.Timestamp == 5678)),
                Times.Once());
        }

        [Fact]
        public async Task InvalidDiceRollsAreNotExecuted()
        {
            var callerMock = new Mock<ILiveClient>();
            clientsMock.Setup(m => m.Caller).Returns(callerMock.Object);

            callerMock
                .Setup(a => a.ExceptionRaised(It.IsAny<string>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;
            hub.Context = Mock.Of<HubCallerContext>();

            var roll = new RollDTO
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "aaaaaaaaa"
            };

            await hub.RollDice(roll);

            callerMock.Verify(
                c => c.ExceptionRaised(It.Is<string>(s => s.Contains("Invalid dice roll"))),
                Times.Once());
        }

        [Fact]
        public async Task MovesCharacter()
        {
            campaignProviderMock
                .Setup(cp => cp.MoveCharacter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<double>(), It.IsAny<double>()))
                .Returns(Task.FromResult(1L));
            
            var submittedStep = new SessionDelta
            {
                CampaignId = "test-campaign",
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "character1",
                        Patch = new JsonPatchDocument().Add("/token/pos", JObject.Parse("{'q': 1, 'r': 2}")),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            campaignProviderMock.Verify(
                c => c.MoveCharacter(
                    It.Is<string>(campaignId => campaignId == submittedStep.CampaignId),
                    It.Is<string>(id => id == "character1"),
                    It.Is<double>(x => x == 1),
                    It.Is<double>(y => y == 2)),
                Times.Once);
        }
    }
}
