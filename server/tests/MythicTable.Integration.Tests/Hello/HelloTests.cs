using System;
using Mongo2Go;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MythicTable.Integration.Tests
{
    public class HelloTests : IClassFixture<WebApplicationFactory<MythicTable.Startup>>
    {
        private readonly WebApplicationFactory<MythicTable.Startup> _factory;

        public HelloTests(WebApplicationFactory<MythicTable.Startup> factory)
        {
            _factory = factory;
            var runner = MongoDbRunner.Start();
            Environment.SetEnvironmentVariable("MTT_MONGODB_CONNECTIONSTRING", runner.ConnectionString);
            Environment.SetEnvironmentVariable("MTT_MONGODB_DATABASENAME", "mythictable");
        }

        [Fact]
        public async Task HelloTest()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/hello");

            response.EnsureSuccessStatusCode();
            Assert.Equal("text/plain; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task HelloMeRequiresAuthTest()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/hello/me");

            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        //[Fact]
        private async Task HelloMeReturnUsernameTest()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/hello/me");

            response.EnsureSuccessStatusCode();
            Assert.Equal("text/plain; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
            var result = await response.Content.ReadAsStringAsync();
            Assert.Equal("hello tester", result);
        }
    }
}