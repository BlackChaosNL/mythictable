using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MythicTable.Campaign.Data;

namespace MythicTable.Campaign.Controllers
{
    [Route("api/campaigns")]
    [ApiController]
    [Authorize]
    public class CampaignController : ControllerBase
    {
        private readonly ICampaignProvider campaignProvider;

        public CampaignController(ICampaignProvider campaignProvider)
        {
            this.campaignProvider = campaignProvider ?? throw new ArgumentNullException(nameof(campaignProvider));
        }

        // GET: api/Campaigns
        [HttpGet]
        public async Task<ActionResult<List<CampaignDTO>>> GetCampaigns()
        {
            var campaigns = await this.campaignProvider.GetAll();

            return campaigns.Select(campaign => campaign as CampaignDTO).ToList();
        }

        // GET: api/Campaigns/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CampaignDTO>> GetCampaign(string id)
        {
            var campaign = await this.campaignProvider.Get(id);

            return campaign as CampaignDTO;
        }

        // PUT: api/Campaigns/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut()]
        public async Task<IActionResult> PutCampaign(CampaignDTO campaign)
        {
            await campaignProvider.Update(campaign);

            return NoContent();
        }

        // POST: api/Campaigns
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CampaignDTO>> PostCampaign(CampaignDTO campaign)
        {
            var player = GetCurrentUser();

            var createdCampaign = await campaignProvider.Create(campaign, player);

            var characters = new List<CharacterDTO>()
            {
                CreateCharacter("marc.png", 7, 18),
                CreateCharacter("sarah.png", 8, 18),
                CreateCharacter("mirko.png", 7, 19),
                CreateCharacter("jon.png", 8, 19),
                CreateCharacter("Redcap.png", 31, 0),
                CreateCharacter("Redcap.png", 31, 0),
                CreateCharacter("Redcap.png", 31, 0),
                CreateCharacter("Wolf.png", 31, 1),
                CreateCharacter("Wolf.png", 31, 1),
                CreateCharacter("Wolf.png", 31, 1),
                CreateColorToken("red", 31, 2),
                CreateColorToken("red", 31, 2),
                CreateColorToken("red", 31, 2),
                CreateColorToken("red", 31, 2),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("black", 31, 3),
                CreateColorToken("blue", 31, 4),
                CreateColorToken("blue", 31, 4),
                CreateColorToken("blue", 31, 4),
                CreateColorToken("blue", 31, 4),
            };
            await campaignProvider.AddCharacters(campaign.Id, characters);

            return CreatedAtAction(nameof(PostCampaign), new { id = campaign.Id }, createdCampaign as CampaignDTO);
        }

        // DELETE: api/Campaigns/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CampaignDTO>> DeleteCampaign(string id)
        {
            var campaign = await campaignProvider.Get(id);
            await campaignProvider.Delete(id);
            return campaign as CampaignDTO;
        }  
        
        [HttpPut("{id}/join")]
        public async Task<ActionResult<CampaignDTO>> Join(string id)
        {
            var player = GetCurrentUser();

            return await campaignProvider.AddPlayer(id, player) as CampaignDTO;
        }
        
        [HttpPut("{id}/leave")]
        public async Task<ActionResult<CampaignDTO>> Leave(string id)
        {
            var player = GetCurrentUser();

            return await campaignProvider.RemovePlayer(id, player) as CampaignDTO;
        }

        // GET: api/campaigns/5/characters
        [HttpGet("{id}/characters")]
        public async Task<List<CharacterDTO>> GetCharacters(string id)
        {

            return await campaignProvider.GetCharacters(id);
        }

        // GET: api/campaigns/5/rolls
        [HttpGet("{id}/rolls")]
        public async Task<List<RollDTO>> GetRolls(string id)
        {

            return await campaignProvider.GetRolls(id);
        }

        private CharacterDTO CreateCharacter(string image, int q, int r)
        {
            return new CharacterDTO{
                token = new CharacterDTO.Token{
                    image =  new CharacterDTO.Token.Image(),
                    pos = new CharacterDTO.Token.Pos{
                        q = q,
                        r = r
                    }
                },
                asset = new CharacterDTO.Asset{
                    src = "/static/assets/" + image
                }
            };
        }
        
        private CharacterDTO CreateColorToken(string color, int q, int r)
        {
            return new CharacterDTO{
                token = new CharacterDTO.Token{
                    image =  new CharacterDTO.Token.Image(color),
                    pos = new CharacterDTO.Token.Pos{
                        q = q,
                        r = r
                    }
                }
            };
        }

        private PlayerDTO GetCurrentUser()
        {
            return new PlayerDTO
            {
                Name = this.HttpContext.User.FindFirst("name")?.Value
            };
        }
    }
}
