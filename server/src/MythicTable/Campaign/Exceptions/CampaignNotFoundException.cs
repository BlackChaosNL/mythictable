using System.Net;

namespace MythicTable.Campaign.Exceptions
{
    public class CampaignNotFoundException : CampaignException
    {
        public CampaignNotFoundException(string msg) : base(msg) { }

        public override HttpStatusCode StatusCode => HttpStatusCode.NotFound;
    }
}