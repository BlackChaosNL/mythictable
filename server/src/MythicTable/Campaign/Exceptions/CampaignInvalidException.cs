namespace MythicTable.Campaign.Exceptions
{
        public class CampaignInvalidException : CampaignException
        {
            public CampaignInvalidException(string msg): base(msg) {}
        }
}