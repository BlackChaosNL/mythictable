namespace MythicTable.Campaign.Exceptions
{
        public class CampaignAddPlayerException : CampaignException
        {
            public CampaignAddPlayerException(string msg): base(msg) {}
        }
}